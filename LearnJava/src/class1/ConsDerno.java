package class1;

class MyClass {
	int x;
	
	MyClass(int i){
		x=i;
	}
}

public class ConsDerno {
	public static void main(String args[]){
		MyClass t1=new MyClass(10);
		MyClass t2=new MyClass(19);
		
		System.out.println(t1.x + " "+ t2.x);
	}

}
