package class1;

class TwoDShape1{
	private double width;
	private double height;
	
	TwoDShape1(double w, double h){
		width=w;
		height=h;
	}
	
	double getWidth(){return width;}
	double getHeight(){return height;}
	
	void setWidth(double w){width=w;}
	void setHeight(double h){height=h;}
	
	void showDim(){
		System.out.println("������ � ������-"+width+"�"+height);
	}
}


class Triangle1 extends TwoDShape1{
	private String style;
	
	Triangle1(String s,double w, double h){
		super(w,h);
		style=s;
	}
	double area(){
		return getWidth()*getHeight()/2;
	}
	void showStyle(){
		System.out.println("����������"+style);
	}
}


public class Shapes4 {
	public static void main(String args[]){
		Triangle1 t1=new Triangle1("�����������",4.0,4.0);
		Triangle1 t2=new Triangle1("���������",8.0,12.0);
		
		System.out.println("���������� � t1: ");
		t1.showStyle();
		t1.showDim();
		System.out.println("������� - "+ t1.area());
		System.out.println();
		System.out.println("���������� � t2: ");
		t2.showStyle();
		t2.showDim();
		System.out.println("������� - "+t2.area());
	}
}
