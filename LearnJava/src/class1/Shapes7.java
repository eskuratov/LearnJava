package class1;
class TwoDShape3{
	private double width;
	private double height;
	
	TwoDShape3(){
		width=height=0.0;
	}
	
	TwoDShape3(double w, double h){
		width=w;
		height=h;
	}
	
	TwoDShape3(double x){
		width=height=x;
	}
	
	TwoDShape3(TwoDShape3 ob){
		width=ob.width;
		height=ob.height;
	}
	
	double getWidth(){return width;}
	double getHeight(){return height;}
	void setWidth(double w){width=w;}
	void setHeight(double h){height=h;}
	
	void showDim(){
		System.out.println("������ � ������ - "+width+"�"+height);
	}
		
}

class Triangle2 extends TwoDShape3{
	private String style;
	
	Triangle2(){
		super();
		style="none";
	}
	
	Triangle2(String s, double w, double h){
		super(w,h);
		style=s;
	}
	
	Triangle2(double x){
		super(x);
		style="�����������";
	}
	
	Triangle2(Triangle2 ob){
		super(ob);
		style=ob.style;
	}
	
	double area(){
		return getWidth()*getHeight()/2;
	}
	void showStyle(){
		System.out.println("�����������"+style);
	}
}

public class Shapes7 {
	public static void main(String args[]) {
		Triangle2 t1=new Triangle2("���������",8.0,12.0);
		Triangle2 t2=new Triangle2(t1);
		System.out.println("���������� � t1: ");
		t1.showStyle();
		t1.showDim();		
		System.out.println("������� - "+t1.area());		
		System.out.println();		
		System.out.println("���������� � t2: ");
		t2.showStyle();
		t2.showDim();
		System.out.println("������� - "+t2.area());		
	}
}
