package class1;

class MyClass1 {
	private int alpha;
	public int beta;
	int gamma;
	
	void setAlpha(int a){
		alpha=a;
	}
	
	int getAlpha(){
		return alpha;
	}
}

class AccessDemo {
	
	public static void main(String args[]){
		
		MyClass1 ob=new MyClass1();
		System.out.println("ob.alpha: "+ob.getAlpha());
		
		ob.beta=88;
		ob.gamma=99;
		
		System.out.println("ob.beta: "+ob.beta);
		
		ob.setAlpha(15);
		
		System.out.println("ob.alpha: "+ob.getAlpha());
	}
	

}
